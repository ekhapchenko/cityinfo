# Реализовать функционал получения информации о городе посредством Python.
#
# User Story: При запуске файла пользователь вводит название города, система возвращает название города, страну, валюту
# и количество населения
#
# Tech Requirements:
#
# Ввод реализовать с помощью CLI интерфейса. Выбор WEB-API для получения информации - на усмотрение разработчика.
# Код должен быть читаемым, с комментариями и соответствовать принципам DRY, KISS, YAGNI.
# Кд должен быть загружен на GitHub или GitLab как отдельный проект с публичным доступом.
# Формат вывода только такой как в примере


import requests
import click


class CityInfo:

    """Class for getting information about the city using WEB-API
    https://wirefreethought.github.io/geodb-cities-api-docs/"""

    city_name = ''
    result_raw = ''
    parsed_data = ''
    params = {}
    country = ''
    currency = ''
    city_info = {}
    url = ''
    result_output = ''

    def __init__(self, user_city):

        """The user specifies the argument city_name"""

        self.city_name = user_city

    def print_result(self):

        """Method for displaying the result"""

        print(f'--------------\n{self.result_output}\n==============')

    def make_request(self):

        """Trying to execute a request with the given url and parameters"""

        try:
            self.result_raw = requests.get(self.url, params=self.params)
        except Exception:
            self.result_output = 'System Error'
            self.print_result()

    def parse_response(self):

        """Upon successful request processing (response code 200-299), receive data in json format"""

        if 300 > self.result_raw.status_code and self.result_raw.status_code >= 200:
            self.parsed_data = self.result_raw.json()
        else:
            self.result_output = 'System Error'
            self.print_result()

    def make_response_for_user(self):

        """Extract information with the key 'data', output the
        necessary information about the city to the terminal"""

        self.city_info = self.parsed_data.get('data')
        for line in self.city_info:
            self.result_output = line.get('city') + '\n'*2
            self.country = line.get('country') + '\n'
            self.result_output += self.country
            self.get_currency()  # the city currency is retrieved using the get_currency method
            self.result_output += self.currency[0] + '\n'
            self.result_output += str(line.get('population'))
            self.print_result()

    def main_job(self):

        """Defining the URL and request parameters, calling methods for executing the request,
        receiving data and displaying the result. Runs in a loop as the result is output with
         a maximum of 10 values"""

        offset = 0  # The zero-ary offset index into the results
        print('>')
        while True:
            self.params = {
                'offset': offset,
                'namePrefix': self.city_name}
            self.url = 'http://geodb-free-service.wirefreethought.com/v1/geo/cities?limit=10'
            self.make_request()
            if not self.result_raw:
                break
            self.parse_response()
            if not self.parsed_data:
                break
            self.make_response_for_user()
            if not self.city_info:
                # if there is no information about the city with a zero offset of the result,
                # then an error message about the city name is displayed
                if offset == 0:
                    self.result_output = self.city_name + '\n'*2
                    self.result_output += 'Invalid city name: ' + self.city_name
                    self.print_result()
                break
            offset += 10  # offset the result by 10
            self.params.update({'offset': offset})

    def get_currency(self):

        """Method of obtaining city currency by country parameter"""

        self.params = {
            'namePrefix': self.country}
        self.url = 'http://geodb-free-service.wirefreethought.com/v1/geo/countries?limit=5&offset=0'
        self.make_request()
        self.parse_response()
        country_info = self.parsed_data.get('data')
        self.currency = country_info[0].get('currencyCodes')


@click.command()
@click.argument('user_city')
def main(user_city):

    """This script is for displaying information about cities.
    You need to run it with an argument -
    the name or part of city name in English"""

    city_obj = CityInfo(user_city)
    city_obj.main_job()


if __name__ == "__main__":
    main()
